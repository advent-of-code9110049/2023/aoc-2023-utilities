package aoc2023utilities

func Check(e error) {
	if e != nil {
		panic(e)
	}
}
