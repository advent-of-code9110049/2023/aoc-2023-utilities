package aoc2023utilities

import (
	"bufio"
	"os"
)

func ScanFile(path string) ([]string, error) {
	var scannerDocument []string

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		scannerDocument = append(scannerDocument, scanner.Text())
	}

	return scannerDocument, scanner.Err()
}
